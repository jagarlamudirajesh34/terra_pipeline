provider "aws" {
   region = "ap-south-1"
   profile = "default"
}

resource "aws_vpc" "myvpc" {
  cidr_block = "192.168.4.0/24"
  tags = {
     Name = "myvpc"
  }
}

resource "aws_subnet" "mysubnet" {
  vpc_id = "${aws_vpc.myvpc.id}"
  cidr_block = "192.168.4.0/25"
  tags = {
     Name = "mysubnet"
   }
}

resource "aws_security_group" "mysg" {
   vpc_id = "${aws_vpc.myvpc.id}"
   tags = {
   	Name = "mysg"
   }  
   ingress {
      protocol = "tcp"
      from_port = 22
      to_port = 22
      cidr_blocks = ["0.0.0.0/0"]
   }
   egress {
      protocol = "tcp"
      from_port = 0
      to_port = 0
      cidr_blocks = ["0.0.0.0/0"]
   }
}

resource "aws_instance" "myec2" {
  ami = "ami-02e60be79e78fef21" 
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.mysubnet.id}"
  security_groups = ["${aws_security_group.mysg.id}"] 
  tags = {
     Name = "Jenkins-terra"
   }
}

